import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity
} from "react-native";

const { width, height } = Dimensions.get("window");
const ComponentItem = ({ content, colorBg, colorText, onPress }) => {
  return (
    <TouchableOpacity
      style={[styles.itemStyle, { backgroundColor: colorBg }]}
      activeOpacity={0.7}
      onPress={onPress}
    >
      <Text style={{ color: colorText, fontSize: 30 }}>{content}</Text>
    </TouchableOpacity>
  );
};

export var countPoint = 0;
export var countOperator = 0;
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      display: "",
      result: "",
      preVal: "",
      operate: "",
      arrayKeyboard: [
        { content: "C", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "+/-", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "%", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "÷", colorBg: "#f78b11", colorText: "#fff" },
        { content: "7", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "8", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "9", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "x", colorBg: "#f78b11", colorText: "#fff" },
        { content: "4", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "5", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "6", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "-", colorBg: "#f78b11", colorText: "#fff" },
        { content: "1", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "2", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "3", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "+", colorBg: "#f78b11", colorText: "#fff" },
        { content: "0", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "Del", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: ".", colorBg: "#c9cacc", colorText: "#1f1f1f" },
        { content: "=", colorBg: "#f78b11", colorText: "#fff" }
      ],
    };
  }

  handleOnPress(item) {
    const { content } = item;
    const { result } = this.state;
    const { operate } = this.state;
    const { preVal } = this.state;
    const { display } = this.state;
    if (
      content === "0" ||
      content === "1" ||
      content === "2" ||
      content === "3" ||
      content === "4" ||
      content === "5" ||
      content === "6" ||
      content === "7" ||
      content === "8" ||
      content === "9"      
    ) {
      this.setState({ result: result + item.content });
      this.setState({ display: display + item.content });
    } else if(
      content === "+" ||
      content === "-" ||
      content === "x" ||
      content === "÷"
    ){
      this.setState({preVal: result});
      this.setState({ result: "" });
      this.setState({operate: item.content});
      this.setState({ display: display + item.content });
    }else if(content === "="){
      var s;
      var i = parseInt(preVal);
      console.log(i);
      var nextVal = parseInt(result)
      console.log(nextVal);
      if(operate =="+"){
       s = i + nextVal;
      } else if(operate =="-"){
        s = i - nextVal;
      }else if(operate =="x"){
        s = i * nextVal;
      }else if(operate =="÷"){
        s = i / nextVal;
      }
      console.log(s);
      this.setState({ display: item.content + s });
    }else {
      switch (content) {
        case 'C':
          this.setState({ display: '' });
          this.setState({ result: '' });
          countPoint = 0;
          break;
        case '.':
          if (countPoint < 1) this.setState({ result: display + item.content})
          countPoint++;
          console.log(countPoint);
        default:
          break;
      }
    }
  }
  render() {
    const { arrayKeyboard } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.areaResult}>
          <Text style={styles.txtResult}>{this.state.display}</Text>
        </View>
        <View style={styles.areaKeyboard}>
          {arrayKeyboard.map(item => (
            <View key={item.content}>
              <ComponentItem
                content={item.content}
                colorBg={item.colorBg}
                colorText={item.colorText}
                // key={item.content}
                onPress={() => this.handleOnPress(item)}
              />
            </View>
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#202020"
  },
  areaResult: {
    flex: 2,
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  txtResult: {
    color: "#fff",
    fontSize: 60
  },
  areaKeyboard: {
    flex: 5,
    flexDirection: "row",
    backgroundColor: "#fff",
    flexWrap: "wrap"
  },
  itemStyle: {
    height: height / 7,
    width: width / 4,
    justifyContent: "center",
    alignItems: "center",
    borderLeftWidth: 1,
    borderLeftColor: "#6a6a6c",
    borderBottomColor: "#6a6a6c",
    borderBottomWidth: 1
  }
});
